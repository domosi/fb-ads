part of newlife;

//Cookies are not enabled on your browser
class FacebookLoginTester {
  Future<dynamic> phantomTest() async {
    await Phantom.start('9898');

    Page page = await Page.create();
    await page.open('https://www.facebook.com', null);
    page.String title = await page.evaluate('function(){ return document.title; }');
    print('Page title is "${title}"');
    //await page.render('google.png');
    page.close();

    // TODO:
//  var elem = page.querySelector('#someId');
//  elem.click();
//  List<Attribute> attributes = elem.attributes;

    Phantom.stop();
  }

  Future<List<Cookie>> openFbMainPage() {
    var completer = new Completer<List<Cookie>>();

    HttpClient client = new HttpClient()
      ..badCertificateCallback = (certificate, host, callbackPort) {
        return true;
      };

    var loginUrl = "https://www.facebook.com";
    Future<HttpClientRequest> open = client.postUrl(Uri.parse(loginUrl));
    open.then((HttpClientRequest request) {
      print(request.uri);

//        request.headers.set('origin', 'https://www.facebook.com');
      //request.headers.set('cookie','datr=a0g6VVs24dKOWgOCk_UkaeBP; fr=0LgSdvMaNtmpNkgOU.AWX25v16c1HzK_ak_2Z0R46Xg4I.BVsK2a.CB.AAA.0.AWXI3Fy5; locale=en_US; lu=RAVhVxRkymtcMZ1c-ZqKFtig; reg_fb_ref=https%3A%2F%2Fwww.facebook.com%2F%3Fstype%3Dlo%26jlou%3DAfctoC33lX5nICgzPKUOA4SOsXDThSP9O0PYY3PUMrUTn2Ooo59DNEF5bLsemo3CuU_UJcrhn9zZXcdSTYPVcttME5a3VRxHnVTp8LC2Hb25Dg%26smuh%3D361%26lh%3DAc9xej8Mbrvlridf; reg_fb_gate=https%3A%2F%2Fwww.facebook.com%2F%3Fstype%3Dlo%26jlou%3DAfctoC33lX5nICgzPKUOA4SOsXDThSP9O0PYY3PUMrUTn2Ooo59DNEF5bLsemo3CuU_UJcrhn9zZXcdSTYPVcttME5a3VRxHnVTp8LC2Hb25Dg%26smuh%3D361%26lh%3DAc9xej8Mbrvlridf; wd=1839x346');
//        request.headers.set('upgrade-insecure-requests', '1');
      request.headers.set('user-agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2471.2 Safari/537.36');
      request.headers.set('content-type', 'text/html');
      request.headers.set('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8');
//        request.headers.set('referer', 'https://www.facebook.com');
//        request.headers.set('origin', 'https://www.facebook.com');
      return request.close();
    }).then((HttpClientResponse response) {
      //print("Got response ");
      response.headers;

      response.headers.forEach((h, l) {
        print("headers: " + h);
        //l.forEach( (v) => print(v));
      });
      //
      response.cookies.forEach((cookie) {
        print("reponse cookie: " + cookie.toString());
      });
      //
      String result = "";
      response.transform(UTF8.decoder).listen((contents) {
        if (contents != null) {
          result = result + contents;
        }
      }, onDone: () {
        completer.complete(response.cookies);
      });
    });

    return completer.future;
  }

  void testPageLoad(String email, List<Cookie> mainPageCookies) {
//    var document = parse('<body>Hello world! <a href="www.html5rocks.com">HTML5 rocks!');
//    print(document.outerHtml);

    String password = "P${new Random().nextInt(100000)}";

    //----
    HttpClient client = new HttpClient()
      ..badCertificateCallback = (certificate, host, callbackPort) {
        return true;
      };

    var loginUrl = "https://www.facebook.com/login.php?login_attempt=1";
    Future<HttpClientRequest> open = client.postUrl(Uri.parse(loginUrl));
    open.then((HttpClientRequest request) {
      print(request.uri);

      request.headers.set('origin', 'https://www.facebook.com');
      request.headers.set('upgrade-insecure-requests', '1');
      request.headers.set('user-agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2471.2 Safari/537.36');
      request.headers.set('content-type', 'application/x-www-form-urlencoded');
      request.headers.set('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8');
      request.headers.set('referer', 'https://www.facebook.com');
      request.headers.set('origin', 'https://www.facebook.com');

      //cookies
//      request.headers.set('cookie', 'datr=a0g6VVs24dKOWgOCk_UkaeBP; fr=0LgSdvMaNtmpNkgOU.AWX25v16c1HzK_ak_2Z0R46Xg4I.BVsK2a.CB.AAA.0.AWXI3Fy5; locale=en_US; lu=RAVhVxRkymtcMZ1c-ZqKFtig; reg_fb_ref=https%3A%2F%2Fwww.facebook.com%2F%3Fstype%3Dlo%26jlou%3DAfctoC33lX5nICgzPKUOA4SOsXDThSP9O0PYY3PUMrUTn2Ooo59DNEF5bLsemo3CuU_UJcrhn9zZXcdSTYPVcttME5a3VRxHnVTp8LC2Hb25Dg%26smuh%3D361%26lh%3DAc9xej8Mbrvlridf; reg_fb_gate=https%3A%2F%2Fwww.facebook.com%2F%3Fstype%3Dlo%26jlou%3DAfctoC33lX5nICgzPKUOA4SOsXDThSP9O0PYY3PUMrUTn2Ooo59DNEF5bLsemo3CuU_UJcrhn9zZXcdSTYPVcttME5a3VRxHnVTp8LC2Hb25Dg%26smuh%3D361%26lh%3DAc9xej8Mbrvlridf; wd=1839x346');
      request.cookies.addAll(mainPageCookies);

      var emailEncoded = Uri.encodeComponent(email);
      var passwordEncoded = Uri.encodeComponent(password);

      request.write(
          "lsd=AVr188JQ&email=${emailEncoded}&pass=${passwordEncoded}&default_persistent=0&timezone=-120&lgndim=eyJ3IjoxODY1LCJoIjoxMDkyLCJhdyI6MTg2NSwiYWgiOjEwNDgsImMiOjI0fQ%3D%3D&lgnrnd=013732_RoVv&lgnjs=1438936503&locale=en_US&qsstamp=W1tbMTMsMzAsMzMsNjgsODUsOTksMTU5LDE2MiwxNjUsMTcwLDIwNCwyMDcsMjI0LDIyOCwyNDMsMjY5LDI5NiwzMDAsMzA3LDMxMiwzNjgsMzg3LDM5OCw0MTEsNDE5LDQzNiw0NjEsNDcyLDQ3Myw0ODYsNDg4LDUwMCw1MTYsNTE3LDUxOSw1MzQsNTUyLDU2Myw2MTksNjU4LDY3NCw2NzVdXSwiQVpuWkFkOVdWemt1b1lJN01KU3NlXzdzcjN3bzVTcW85WHVhVktqM0pBWmJ0TFVDQi1QSWpudGFRLVpMcG5CTW5hNzNFMXFJOS0ybDVsRTBhQmxtNzlCN0NVTFBkVHUxSkZGSmRkWDd3RHdvNnJkU0xycHYyNGVwM1pGVjlLaDBCS21QV2hYcHp5UEJQUGVCTjYzMzlUWmMyZ2x1MGVBYjJsUGJYQVhXVmtnSmFoUzFJMzZTb3g0WTB1SndIQnNlYUpyQW5rQUktNEExVExJT0JqeURvemdwdExZQWdITld3QzZmNVI1bzlLbmNRUSJd");

      return request.close();
    }).then((HttpClientResponse response) {
      //print("Got response ");
      response.headers;

      response.headers.forEach((h, l) {
        //print("headers: " + h);
        //l.forEach( (v) => print(v));
      });
//
//      response.cookies.forEach((cookie) {
//        //print("reponse cookie: " + cookie.toString());
//      });
//
      String result = "";
      response.transform(UTF8.decoder).listen((contents) {
        if (contents != null) {
          result = result + contents;
        }
      }, onDone: () {
        if (result.contains("Please Confirm Password")) {
          print("Please Confirm Password ");
        } else if (result.contains("Please re-enter your password")) {
          print("Please re-enter your password");
        } else if (result.contains("Incorrect Email")) {
          print("Incorrect Email");
        } else {
          print(" ------   MÁS ------");
          print(result);
        }
      });
    });
  }
}

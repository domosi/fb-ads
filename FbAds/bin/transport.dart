part of FbAds;

class Transport {
  final fbBaseUri = "https://graph.facebook.com/v2.2";
  String accessToken;

  Transport(String accessToken) {
    this.accessToken = accessToken;
  }

  void callApi(String endpoint, var body, Function callback(var object)) {

    var url = "$fbBaseUri/$endpoint";
    if(accessToken != null) {
      if (endpoint.contains('?')) {
        url = "$url&access_token=$accessToken";
      } else {
        url = "$url?access_token=$accessToken";
      }
    }

    String result = "";

    HttpClient client = new HttpClient()..badCertificateCallback = (certificate, host, callbackPort) {
          return true;
        };

    Future<HttpClientRequest> open;
    if (body == null) {
      open = client.getUrl(Uri.parse(url));
    } else {
      open = client.postUrl(Uri.parse(url));
    }
    open.then((HttpClientRequest request) {
      print(request.uri);

      if (body != null) {
        request.headers.set(HttpHeaders.CONTENT_TYPE, 'application/json');
        request.write(JSON.encode(body));
      }

      return request.close();

    }).then((HttpClientResponse response) {
      //print("Got response ");

      response.headers.forEach((h, l) {
        //print("headers: " + h);
        //l.forEach( (v) => print(v));
      });

      response.cookies.forEach((cookie) {
        //print("reponse cookie: " + cookie.toString());
      });

      response.transform(UTF8.decoder).listen((contents) {
        if (contents != null) {
          result = result + contents;
        }
      }, onDone: () {
        if (callback != null) {
          if (!response.headers['content-type'].contains('json')) {
            callback(result);
          } else {
            callback(JSON.decode(result));
          }
        }
      });
    });
  }
/*
  Object callApiSync (String endpoint, var body, Function callback(var object)) async {

    var url = "$fbBaseUri/$endpoint";
    if(accessToken != null) {
      if (endpoint.contains('?')) {
        url = "$url&access_token=$accessToken";
      } else {
        url = "$url?access_token=$accessToken";
      }
    }

    HttpClient client = new HttpClient()..badCertificateCallback = (certificate, host, callbackPort) {
          return true;
        };

    HttpClientRequest request;
    if (body == null) {
      request = await client.getUrl(Uri.parse(url));
    } else {
      request = await client.postUrl(Uri.parse(url));
    }

    print(request.uri);

    if (body != null) {
      request.headers.set(HttpHeaders.CONTENT_TYPE, 'application/json');
      request.write(JSON.encode(body));
    }

    HttpClientResponse response = await request.close();
    //print("Got response ");

    response.headers.forEach((h, l) {
      //print("headers: " + h);
      //l.forEach( (v) => print(v));
    });

    response.cookies.forEach((cookie) {
      //print("reponse cookie: " + cookie.toString());
    });

    final StringBuffer result = new StringBuffer();
    var returnValue;


    String got = await response.transform(UTF8.decoder).listen(
        (contents) {
          if (contents != null) {
            result.write(contents);
          }

          print("listen " + contents);
        },
        onDone: () {
            if (!response.headers['content-type'].contains('json')) {
              returnValue = result;
            } else {
              returnValue = JSON.decode(result.toString());
            }

            print("returnValue" + returnValue);
        }).asFuture(result.toString()).then((r) {
          print("then " + r);
        });


    print("vege ");

    return returnValue;
  }*/

}

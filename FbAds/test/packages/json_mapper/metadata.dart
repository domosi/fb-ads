part of json_mapper;


class IgnoreProperty {
  const IgnoreProperty();
}

class IgnoreProperties {
  final List<String> fields;
  const IgnoreProperties(this.fields);
}
